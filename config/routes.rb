Rails.application.routes.draw do
  get 'role_managers/index'
  devise_for :users
  resources :articles do
    resources :comments
  end
  resources :role_managers, only:[:index, :edit]
  root 'articles#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
