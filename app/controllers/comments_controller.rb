class CommentsController < ApplicationController
    before_action:authenticate_user! 
    def create
        @article=Article.find(params[:article_id]) 
        @comment= @article.comments.new(comment_params)
        @comment.commenter=current_user.email
        if @comment.save
            redirect_to article_path(@article)
        end
    end

    def destroy
        @article=Article.find(params[:article_id])
        @comment=@article.comments.find(params[:id])
        @comment.destroy
        redirect_to article_path(@article)
    end

    private
        def comment_params
            params.require(:comment).permit(:body,:status)
        end
end
