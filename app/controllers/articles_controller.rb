class ArticlesController < ApplicationController
    before_action:authenticate_user! , except: [:index,:show] 
    load_and_authorize_resource 

  def index
    @status= params[:status]? params[:status] : 'public'
    @articles=Article.all.where(:status => @status).paginate(page: params[:page], per_page:2).order('created_at DESC')
end

def show
    @article=Article.find(params[:id])
end

def new
    @article=Article.new
end

def create 
    @article=current_user.articles.new(article_params)
    if @article.save
        redirect_to root_path  
    else
        render :new
    end
end

def edit
    @article=Article.find(params[:id])
end
def update
    @article=Article.find(params[:id])
    if @article.update(article_params)
        redirect_to @article
    else
        render :edit
    end
end

def destroy
    @article=Article.find(params[:id])
    @article.destroy
    redirect_to articles_path
end

private
    def article_params
        params.require(:article).permit(:title,:body,:status)
    end
end
