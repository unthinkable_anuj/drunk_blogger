class RoleManagersController < ApplicationController
  def index
    @user=User.all
  end

  def edit
    @user=User.find(params[:id])
    if @user.has_role? :admin 
        @user.remove_role :admin
    else
      @user.add_role :admin
    end
    redirect_to role_managers_index_path
  end
  
end