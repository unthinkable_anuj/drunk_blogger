class DelColumnAuthorId < ActiveRecord::Migration[6.1]
  def change
    remove_column :articles, :author_id, :integer
  end
end
